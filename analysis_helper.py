#Load CCF fucntions
import math
import numpy as np
import scipy as sp
from scipy import ndimage
from scipy.interpolate import bisplev,bisplrep
from scipy.fftpack import dct

def cart2polar(x, y):
    r = np.sqrt(x**2 + y**2)
    theta = np.arctan2(y, x)
    return r, theta


def polar2cart(r, theta):
    x = r * np.cos(theta)
    y = r * np.sin(theta)
    return x, y


def reproject_polar_into_cart(polar_data, cval=0.0):
    nr, nt = polar_data.shape[:2]
    nx = ny = 2*nr

    xi = np.linspace(-nx/2, nx/2, nx, endpoint=False)
    yi = xi.copy()

    x_grid, y_grid = np.meshgrid(xi, yi)

    r_grid, theta_grid = cart2polar(x_grid, y_grid)

    theta_grid += np.pi
    theta_grid *= (nt-1)/(2*np.pi)

    ri, ti = r_grid.flatten(), theta_grid.flatten()
    coords = np.vstack((ri, ti))

    zi = ndimage.map_coordinates(polar_data, coords, order=1, cval=cval)

    return zi.reshape((ny, nx))


def getXiYiForPolarCoords(shape, rmin=0, rmax=0):
    ny, nx = shape

    origin = (ny/2.0, nx/2.0)

    # distance to the most far corner
    tmax = np.pi
    tmin = -np.pi

    # Make a regular (in polar space) grid based on the min and max r & theta
    if rmax == 0:
        rmax = int(math.sqrt((max(ny-origin[0], origin[0]))**2 + \
                          (max(nx-origin[1], origin[1]))**2))
    # nr = int(r_max)
    # nt = 360
    nr = rmax - rmin # Don't change!!
    nt = (nx + ny)//2
    r_i = np.linspace(rmin, rmax, nr)
    theta_i = np.linspace(tmin, tmax, nt)
    theta_grid, r_grid = np.meshgrid(theta_i, r_i)

    xi, yi = polar2cart(r_grid, theta_grid)

    xi, yi = xi.flatten(), yi.flatten()

    return xi,yi,nr,nt

def reprojectImageByCoords(data,xi_in,yi_in,nr,nt,origin=None,cval=0):
    ny, nx = data.shape[-2:]

    if origin == None:
        origin = (ny/2.0, nx/2.0)

    yi = yi_in + origin[0]
    xi = xi_in + origin[1]

    coords = np.vstack((yi, xi))

    zi = ndimage.map_coordinates(data, coords, order=1, cval=cval)
    
    return zi.reshape((nr, nt))

def correlate_by_angle(polar_data,polar_mask = None):
    arr = np.zeros(polar_data.shape)
    if polar_mask is not None:
        polar_data = fill_polar_data_under_mask(polar_data, polar_mask)

    for l,p in enumerate(polar_data):
        arr[l,:] = sp.correlate(p, np.concatenate((p, p)))[:-1]/len(p)
    return arr

def fill_polar_data_under_mask(polar_data, polar_mask):
    rolled_mask = np.roll(polar_mask, polar_mask.shape[1]//2, axis=1)
    rolled_data = np.roll(polar_data, polar_data.shape[1]//2, axis=1).copy()

    for l,p in enumerate(polar_data):
        clip_p = np.clip(p, 0, np.amax(p))
        c = np.count_nonzero(clip_p)
        average_val = 0
        if c!=0:
            average_val = np.sqrt(np.sum(np.power(clip_p,2))/c)

        np.putmask(p,(polar_mask[l,:]<0)*(rolled_mask[l,:]>=0), rolled_data[l,:])
        np.place(p, (polar_mask[l,:]<0)*(rolled_mask[l,:]<0), average_val)
#         np.place(p, (polar_mask[l,:]<0), average_val)

    return polar_data

def combine_ccf_for_angles(ccf_data):
    R, Q = ccf_data.shape[:2]

    ccf1d_ang = np.sum(ccf_data, axis=0)/R

    ccf1d_ang /= ccf1d_ang[0]/1000.0

    return ccf1d_ang

def fill_image_under_mask(data, mask, origin):
    Y,X = data.shape[-2:]
    max_rad = np.sqrt(max(Y-origin[0],origin[0])**2 + max(X-origin[1],origin[1])**2)+1
    xi, yi, nr, nt = getXiYiForPolarCoords(data.shape[-2:], 0, max_rad)
    polar_image = reprojectImageByCoords(data, xi, yi, nr, nt, origin)
    polar_mask = reprojectImageByCoords(mask, xi, yi, nr, nt, origin,cval=-10000)
    with np.errstate(divide='ignore', invalid='ignore'):
        averaged_radial = np.array([np.mean(p[polar_mask[l,:]>=0]) for l,p in enumerate(polar_image)],dtype=np.float32)
        averaged_radial = np.nan_to_num(averaged_radial) # average of empty line is 0
    
    yy,xx = np.indices(data.shape)
    yy -= origin[0]
    xx -= origin[1]
    
    rr = np.round(np.sqrt(xx**2 + yy**2)).astype(int)
    np.putmask(data, mask<0, np.take(averaged_radial,rr))
    
    return data